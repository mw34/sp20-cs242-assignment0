#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
import scipy.sparse
import scipy.sparse.linalg as la
import numpy.linalg
from utils import *
import os


# In[2]:


toy_img = cv2.cvtColor(cv2.imread('samples/toy_problem.png'), cv2.COLOR_BGR2RGB)
toy_img = toy_img.astype(np.float64)
toy_img = toy_img/255
#print(toy_img[0][0][0].dtype)
#plt.imshow(toy_img)
#shape = toy_img.shape
#print(shape)
##########################
im = cv2.cvtColor(cv2.imread('samples/toy_problem.png'), cv2.COLOR_BGR2GRAY)
im_h, im_w = im.shape
im = im.astype(np.float64)
im = im/255
im2var = np.arange(im_h * im_w).reshape(im_w, im_h).T
plt.imshow(im,cmap='gray')
print(im.shape)
print(im2var[1][0])
toy_img = im


# ## Part 1 Toy Problem (20 pts)

# In[4]:


def toy_reconstruct(toy_img):
    """
    The implementation for gradient domain processing is not complicated, but it is easy to make a mistake, so let's start with a toy example. Reconstruct this image from its gradient values, plus one pixel intensity. Denote the intensity of the source image at (x, y) as s(x,y) and the value to solve for as v(x,y). For each pixel, then, we have two objectives:
    1. minimize (v(x+1,y)-v(x,y) - (s(x+1,y)-s(x,y)))^2
    2. minimize (v(x,y+1)-v(x,y) - (s(x,y+1)-s(x,y)))^2
    Note that these could be solved while adding any constant value to v, so we will add one more objective:
    3. minimize (v(1,1)-s(1,1))^2
    
    :param toy_img: numpy.ndarray
    """
    tmp = im_h * im_w
    A = np.zeros((2 * tmp - im_h - im_w + 1,tmp))
    b = np.zeros(2 * tmp - im_h - im_w + 1)
    e = 0
    for i in range(im_h):
        for j in range(im_w-1):
            A[e][im2var[i][j+1]] = 1
            A[e][im2var[i][j]] = -1
            b[e] = im[i][j+1] - im[i][j]
            e = e+1
    for i in range(im_h-1):
        for j in range(im_w):
            A[e][im2var[i+1][j]] = 1
            A[e][im2var[i][j]] = -1
            b[e] = im[i+1][j] - im[i][j]
            e = e+1
    A[e][im2var[0][0]] = 1
    b[e] = im[0][0]
    e = e+1
    #print(e - (2 * tmp - im_h - im_w + 1))
    #print(A.shape)
    #print(b.shape)
    #print(np.count_nonzero(b))
    A = scipy.sparse.csr_matrix(A)
    #print(A.shape)
    #print(A)
    target =  la.lsqr(A,b,atol = 10**(-14),btol = 10**(-14))
    #target = numpy.linalg.lstsq(A,b)
    print(target[0])
    return target[0]


# In[5]:


im_out = toy_reconstruct(toy_img)
k = np.zeros((im_h,im_w))
for i in range(0,im_h):
    for j in range(0,im_w):
        k[i][j] = im_out[im2var[i][j]]
im_out = k
#if im_out and im_out.any():
print("Error is: ", np.sqrt(((im_out - toy_img)**2).sum()))
plt.imshow(im_out,cmap='gray')


# ## Preparation

# In[86]:


# Feel free to change image
background_img = cv2.cvtColor(cv2.imread('samples/t4.JPG'), cv2.COLOR_BGR2RGB).astype('double') / 255.0 
plt.figure()
plt.imshow(background_img)


# In[87]:


# Feel free to change image
object_img = cv2.cvtColor(cv2.imread('samples/s4.jpg'), cv2.COLOR_BGR2RGB).astype('double') / 255.0 
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'notebook')
mask_coords = specify_mask(object_img)


# In[89]:


xs = mask_coords[0]
ys = mask_coords[1]
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
plt.figure()
mask = get_mask(ys, xs, object_img)
im_h = object_img.shape[0]
im_w = object_img.shape[1]
im2var = np.arange(im_h * im_w).reshape(im_w, im_h).T
print(im_h*im_w)
print(im_h)
print(im_w)
print(im2var[404][44])


# In[90]:


get_ipython().run_line_magic('matplotlib', 'notebook')
import matplotlib.pyplot as plt
bottom_center = specify_bottom_center(background_img)


# In[91]:


def my_align_source(object_img, mask, background_img, bottom_center):
    ys, xs = np.where(mask == 1)
    y1 = min(ys)-1
    y2 = max(ys)+1
    x1 = min(xs)-1
    x2 = max(xs)+1
    object_img2 = np.zeros(background_img.shape)
    yind = np.arange(y1,y2)
    yind2 = yind - int(max(ys)) + bottom_center[1]
    xind = np.arange(x1,x2)
    xind2 = xind - int(round(np.mean(xs))) + bottom_center[0]
    tmpx = xs
    tmpy = ys
    ys = ys - int(max(ys)) + bottom_center[1]
    xs = xs - int(round(np.mean(xs))) + bottom_center[0]
    
    mask2 = np.zeros(background_img.shape[:2], dtype=bool)
    for i in range(len(xs)):
        mask2[int(ys[i]), int(xs[i])] = True
    for i in range(len(yind)):
        for j in range(len(xind)):
            object_img2[yind2[i], xind2[j], :] = object_img[yind[i], xind[j], :]
    mask3 = np.zeros([mask2.shape[0], mask2.shape[1], 3])
    for i in range(3):
        mask3[:,:, i] = mask2
    background_img  = object_img2 * mask3 + (1-mask3) * background_img
    plt.figure()
    plt.imshow(background_img)
    return object_img2, mask2, xs, ys, tmpx,tmpy
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
cropped_object, object_mask,mj,mi,sj,si = my_align_source(object_img, mask, background_img, bottom_center)
#mi,mj = np.nonzero(object_mask)
print(len(mi))
print(len(mj))
print(mj)
print(si)
size = len(mi)


# ## Part 2 Poisson Blending (50 pts)

# In[92]:


plt.imshow(object_mask)
print(object_mask)


# In[94]:


#A[e][im2var[i][j+1]] = 1
#A[e][im2var[i][j]] = -1
#b[e] = im[i][j+1] - im[i][j]
def neighbors(index):
    # index = [i,j]
    x = index[0]
    y = index[1]
    # right-left-up-down list
    return [[x+1,y],[x-1,y],[x,y+1],[x,y-1]]
def if_edge(index):
    for n in neighbors(index):
        if object_mask[n[0]][n[1]] == False:
            return True
    return False
def poisson_blend(cropped_object, object_mask, background_img,color):
    """
    :param cropped_object: numpy.ndarray One you get from align_source
    :param object_mask: numpy.ndarray One you get from align_source
    :param background_img: numpy.ndarray 
    """
    #TO DO 
    #A = np.zeros((3*size,size))
    #b = np.zeros(3*size)
    A = scipy.sparse.lil_matrix((4*size,im_h*im_w))
    #print(A.shape)
    b = np.zeros(4*size)
    e = 0
    co = color
    for i in range(0,size):
        if if_edge([mi[i],mj[i]]):
            for k in neighbors([mi[i],mj[i]]):
                if object_mask[k[0]][k[1]] == False:
                    ### not in the mask:        
                    A[e,im2var[si[i]][sj[i]]] = 1
                # tj + si-sj
                    b[e] = background_img[k[0]][k[1]][co] + object_img[si[i]][sj[i]][co] - object_img[si[i]+k[0]-mi[i]][sj[i]+k[1]-mj[i]][co]
                    e = e+1
        if object_mask[mi[i]][mj[i]+1] == True:
            #print(si[i])
            #print(sj[i])
            A[e,im2var[si[i]][sj[i]+1]] = 1
            A[e,im2var[si[i]][sj[i]]] = -1
            b[e] = object_img[si[i]][sj[i]+1][co] - object_img[si[i]][sj[i]][co]
            e = e+1
        if object_mask[mi[i]+1][mj[i]] == True:
            A[e,im2var[si[i]+1][sj[i]]] = 1
            A[e,im2var[si[i]][sj[i]]] = -1
            b[e] = object_img[si[i]+1][sj[i]][co] - object_img[si[i]][sj[i]][co]
            e = e+1
    A = scipy.sparse.csr_matrix(A)
    #print(A.shape)
    #print(A)
    target_p =  la.lsqr(A,b)
    #target = numpy.linalg.lstsq(A,b)
    #print(np.count_nonzero(target_p[0]))
    return target_p[0]
    pass


# In[ ]:





# In[65]:


im_blend_r = poisson_blend(cropped_object, object_mask, background_img,0)
im_blend_g = poisson_blend(cropped_object, object_mask, background_img,1)
im_blend_b = poisson_blend(cropped_object, object_mask, background_img,2)
#if im_blend and im_blend.any():
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
oo = np.zeros((im_h,im_w,3))
for i in range(0,im_h):
    for j in range(0,im_w):
        oo[i][j][0] = im_blend_r[im2var[i][j]]
        oo[i][j][1] = im_blend_g[im2var[i][j]]
        oo[i][j][2] = im_blend_b[im2var[i][j]]
plt.imshow(oo)
#plt.imshow(im_blend)


# In[66]:


ys, xs = np.where(mask == 1)
y1 = min(ys)-1
y2 = max(ys)+1
x1 = min(xs)-1
x2 = max(xs)+1
object_img2 = np.zeros(background_img.shape)
yind = np.arange(y1,y2)
yind2 = yind - int(max(ys)) + bottom_center[1]
xind = np.arange(x1,x2)
xind2 = xind - int(round(np.mean(xs))) + bottom_center[0]
#tmpx = xs
#tmpy = ys
ys = ys - int(max(ys)) + bottom_center[1]
xs = xs - int(round(np.mean(xs))) + bottom_center[0]    
mask2 = np.zeros(background_img.shape[:2], dtype=bool)
for i in range(len(xs)):
    mask2[int(ys[i]), int(xs[i])] = True
for i in range(len(yind)):
    for j in range(len(xind)):
        object_img2[yind2[i], xind2[j], :] = oo[yind[i], xind[j], :]
mask3 = np.zeros([mask2.shape[0], mask2.shape[1], 3])
for i in range(3):
    mask3[:,:, i] = mask2
output  = object_img2 * mask3 + (1-mask3) * background_img
plt.figure()
plt.imshow(output)


# ## Part 3 Mixed Gradients (20 pts)

# In[ ]:





# In[95]:


def mix_blend(cropped_object, object_mask, background_img,color):
    """
    :param cropped_object: numpy.ndarray One you get from align_source
    :param object_mask: numpy.ndarray One you get from align_source
    :param background_img: numpy.ndarray 
    """
    #TO DO
    A = scipy.sparse.lil_matrix((4*size,im_h*im_w))
    #print(A.shape)
    b = np.zeros(4*size)
    e = 0
    co = color
    for i in range(0,size):
        if if_edge([mi[i],mj[i]]):
            for k in neighbors([mi[i],mj[i]]):
                if object_mask[k[0]][k[1]] == False:
                    ### not in the mask:        
                    A[e,im2var[si[i]][sj[i]]] = 1
                # tj + si-sj
                    if abs(object_img[si[i]][sj[i]][co] - object_img[si[i]+k[0]-mi[i]][sj[i]+k[1]-mj[i]][co])>abs(background_img[mi[i]][mj[i]][co] - background_img[k[0]][k[1]][co]):
                        ad = object_img[si[i]][sj[i]][co] - object_img[si[i]+k[0]-mi[i]][sj[i]+k[1]-mj[i]][co]
                    else:
                        ad = background_img[mi[i]][mj[i]][co] - background_img[k[0]][k[1]][co] 
                    b[e] = background_img[k[0]][k[1]][co] + ad
                    e = e+1
        if object_mask[mi[i]][mj[i]+1] == True:
            #print(si[i])
            #print(sj[i])
            A[e,im2var[si[i]][sj[i]+1]] = 1
            A[e,im2var[si[i]][sj[i]]] = -1
            if abs(object_img[si[i]][sj[i]+1][co] - object_img[si[i]][sj[i]][co]) > abs(background_img[mi[i]][mj[i]+1][co] - background_img[mi[i]][mj[i]][co]):
                ad = object_img[si[i]][sj[i]+1][co] - object_img[si[i]][sj[i]][co]
            else:
                ad = background_img[mi[i]][mj[i]+1][co] - background_img[mi[i]][mj[i]][co]
            b[e] = ad
            e = e+1
        if object_mask[mi[i]+1][mj[i]] == True:
            A[e,im2var[si[i]+1][sj[i]]] = 1
            A[e,im2var[si[i]][sj[i]]] = -1
            if abs(object_img[si[i]+1][sj[i]][co] - object_img[si[i]][sj[i]][co]) > abs(background_img[mi[i]+1][mj[i]][co] - background_img[mi[i]][mj[i]][co]):
                ad = object_img[si[i]+1][sj[i]][co] - object_img[si[i]][sj[i]][co]
            else:
                ad = background_img[mi[i]+1][mj[i]][co] - background_img[mi[i]][mj[i]][co]
            b[e] = ad
            #b[e] = object_img[si[i]+1][sj[i]][co] - object_img[si[i]][sj[i]][co]
            e = e+1
    A = scipy.sparse.csr_matrix(A)
    #print(A.shape)
    #print(A)
    target_p =  la.lsqr(A,b)
    #target = numpy.linalg.lstsq(A,b)
    #print(np.count_nonzero(target_p[0]))
    return target_p[0]
    pass


# In[96]:


im_blend_r = mix_blend(cropped_object, object_mask, background_img,0)
im_blend_g = mix_blend(cropped_object, object_mask, background_img,1)
im_blend_b = mix_blend(cropped_object, object_mask, background_img,2)
#if im_blend and im_blend.any():
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
oo = np.zeros((im_h,im_w,3))
for i in range(0,im_h):
    for j in range(0,im_w):
        oo[i][j][0] = im_blend_r[im2var[i][j]]
        oo[i][j][1] = im_blend_g[im2var[i][j]]
        oo[i][j][2] = im_blend_b[im2var[i][j]]
plt.imshow(oo)
#plt.imshow(im_blend)


# In[97]:


ys, xs = np.where(mask == 1)
y1 = min(ys)-1
y2 = max(ys)+1
x1 = min(xs)-1
x2 = max(xs)+1
object_img2 = np.zeros(background_img.shape)
yind = np.arange(y1,y2)
yind2 = yind - int(max(ys)) + bottom_center[1]
xind = np.arange(x1,x2)
xind2 = xind - int(round(np.mean(xs))) + bottom_center[0]
#tmpx = xs
#tmpy = ys
ys = ys - int(max(ys)) + bottom_center[1]
xs = xs - int(round(np.mean(xs))) + bottom_center[0]    
mask2 = np.zeros(background_img.shape[:2], dtype=bool)
for i in range(len(xs)):
    mask2[int(ys[i]), int(xs[i])] = True
for i in range(len(yind)):
    for j in range(len(xind)):
        object_img2[yind2[i], xind2[j], :] = oo[yind[i], xind[j], :]
mask3 = np.zeros([mask2.shape[0], mask2.shape[1], 3])
for i in range(3):
    mask3[:,:, i] = mask2
output  = object_img2 * mask3 + (1-mask3) * background_img
plt.figure()
plt.imshow(output)


# # Bells & Whistles (Extra Points)

# ## Color2Gray (20 pts)

# In[ ]:


def color2gray(img):
    pass


# ## Laplacian pyramid blending (20 pts)

# In[ ]:


def laplacian_blend(img1, img2):
    pass


# ## More gradient domain processing (up to 20 pts)

# In[ ]:




